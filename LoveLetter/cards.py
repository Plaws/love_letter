from abc import ABC, abstractmethod
import os

from LoveLetter.colors import (
    blue,
    dark_orange,
    dark_pink,
    dark_violet,
    green,
    grey_blue,
    light_blue,
    pale_yellow,
)


GOLDEN_RATIO = 1.62


class RequiresTarget(Exception):
    def __init__(self, message):
        self.message = message


class RequiresGuess(Exception):
    def __init__(self, message):
        self.message = message


class CountessException(Exception):
    def __init__(self, message):
        self.message = message


class InvalidTarget(Exception):
    def __init__(self, message):
        self.message = message


class Card(ABC):
    card_width = 180
    card_height = int(card_width * GOLDEN_RATIO)
    image_proportion = (card_width, card_height)
    current_dir = os.path.dirname(__file__)
    image_folder = os.path.join(current_dir, 'cards_image')
    default_extension = '.jpg'

    def __init__(self, value, name, card_text, color, image=None):
        self.value = value
        self.name = name
        self.card_text = card_text
        self.color = color
        if not image:
            self.image = os.path.join(
                self.image_folder,
                self.__class__.__name__.lower() + self.default_extension,
            )
        else:
            self.image = os.path.join(
                self.image_folder,
                image,
            )

    def __repr__(self):
        return "({}) {}".format(self.value, self.name)

    def __str__(self):
        return "({}) {}".format(self.value, self.name)

    def __lt__(self, other):
        if self.value < other.value:
            return True
        return False

    def set_owner(self, owner):
        self.owner = owner

    @abstractmethod
    def action(self, **kwargs):
        pass

    @abstractmethod
    def play_allowed(self, **kwargs):
        pass


class Guard(Card):

    def __init__(self):
        Card.__init__(self,
                      1,
                      "Guard",
                      "Choose a player and a non-guard card. If the player "
                      "has this card (s)he is eliminated this round.",
                      dark_orange,
                      )

    def action(self, **kwargs):
        target = kwargs.get("target")
        guess = kwargs.get("guess")
        if target.has_card(guess):
            target.get_eliminated()

    def play_allowed(self, **kwargs):
        target = kwargs.get("target")
        if target is None:
            raise RequiresTarget("Missing target")
        if target.immune:
            raise InvalidTarget("{} is immune".format(target))
        if target.dead:
            raise InvalidTarget("{} is out of the game".format(target))
        if kwargs.get("guess", None) is None:
            raise RequiresGuess("Missing guess")
        if target == self.owner:
            raise InvalidTarget("Can't target yourself with {}"
                                .format(self.name))
        return True


class Priest(Card):

    def __init__(self):
        Card.__init__(self,
                      2,
                      "Priest",
                      "Choose a player and secretely take a look a his card.",
                      dark_pink,)

    def action(self, **kwargs):
        target = kwargs["target"]
        print("Player {}, has {}".format(target, target.get_card()))

    def play_allowed(self, **kwargs):
        target = kwargs.get("target")
        if target is None:
            raise RequiresTarget("Missing target")
        if target.immune:
            raise InvalidTarget("{} is immune".format(target))
        if target.dead:
            raise InvalidTarget("{} is out of the game".format(target))
        if target == self.owner:
            raise InvalidTarget("Can't target yourself with {}"
                                .format(self.name))
        return True


class Baron(Card):

    def __init__(self):
        Card.__init__(self,
                      3,
                      "Baron",
                      "Choose an other player in the round. Compare hands. "
                      "The one with the lower rank is out of the round.",
                      dark_violet,)

    def action(self, **kwargs):
        owner_card = self.owner.get_card()
        target = kwargs["target"]
        if target.immune:
            raise InvalidTarget("{} is immune".format(target))
        if target.dead:
            raise InvalidTarget("{} is out of the game".format(target))
        target_card = target.get_card()
        if (target_card < owner_card):
            target.get_eliminated()
        elif (owner_card < target_card):
            self.owner.get_eliminated()

    def play_allowed(self, **kwargs):
        target = kwargs.get("target")
        if target is None:
            raise RequiresTarget("Missing target")
        if target == self.owner:
            raise InvalidTarget("Can't target yourself with {}"
                                .format(self.name))
        return True


class Handmaiden(Card):

    def __init__(self):
        Card.__init__(self,
                      4,
                      "Handmaiden",
                      "Player who plays this card is protected "
                      "until next turn",
                      light_blue,
                      )

    def action(self, **kwargs):
        self.owner.immune = True
        self.owner.area = None

    # Handmaiden is always allowed
    def play_allowed(self, **kwargs):
        pass


class Prince(Card):

    def __init__(self):
        Card.__init__(self,
                      5,
                      "Prince",
                      "Choose a player (including you), "
                      "(s)he discards and draw a new card.",
                      pale_yellow,)

    def action(self, **kwargs):
        target = kwargs["target"]
        target.discard()

    def play_allowed(self, **kwargs):
        for card in self.owner.hand:
            if type(card) == Countess:
                raise CountessException("You have to play Countess")
        target = kwargs.get("target")
        if target is None:
            raise RequiresTarget("Missing target")
        if target.immune:
            raise InvalidTarget("{} is immune".format(target))
        if target.dead:
            raise InvalidTarget("{} is out of the game".format(target))
        return True


class King(Card):

    def __init__(self):
        Card.__init__(self,
                      6,
                      "King",
                      "Trade your card with someone else.",
                      green,)

    def action(self, **kwargs):
        target = kwargs.get("target")
        self.owner.swap_hand(target)

    def play_allowed(self, **kwargs):
        for card in self.owner.hand:
            if type(card) == Countess:
                raise CountessException("You have to play Countess")
        target = kwargs.get("target")
        if target is None:
            raise RequiresTarget("Missing target")
        if target.immune:
            raise InvalidTarget("{} is immune".format(target))
        if target.dead:
            raise InvalidTarget("{} is out of the game".format(target))
        if target == self.owner:
            raise InvalidTarget("Can't target yourself with {}"
                                .format(self.name))
        return True


class Countess(Card):

    def __init__(self):
        Card.__init__(self,
                      7,
                      "Countess",
                      "If Prince or King is in your hand, you MUST discard "
                      "this.",
                      blue,)

    def action(self, **kwargs):
        pass

    # Countess is always allowed
    def play_allowed(self, **kwargs):
        pass


class Princess(Card):

    def __init__(self):
        Card.__init__(self,
                      8,
                      "Princess",
                      "If you discard the princess, you are out of the round.",
                      grey_blue)

    def action(self, **kwargs):
        self.owner.get_eliminated()

    # Princess is always allowed
    def play_allowed(self, **kwargs):
        pass
