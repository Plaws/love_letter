import pygame


class Pane:

    def __init__(self, screen, coord, color, text="", text_size=50, border=0):
        self.font = pygame.font.SysFont('Arial', text_size)
        self.screen = screen
        self.coord = coord
        self.color = color
        self.text = text
        self.border = border

    def display(self):
        self.displayRect()
        self.displayText()

    def displayRect(self):
        self.area = pygame.draw.rect(
            self.screen,
            self.color,
            self.coord,
            self.border
        )

    def displayText(self):
        text = self.font.render(self.text, True, (0, 0, 0))
        center_coord = (self.coord[0] + self.coord[2] / 2,
                        self.coord[1] + self.coord[3] / 2)
        text_rect = text.get_rect(center=center_coord)
        self.screen.blit(text, text_rect)
