import pytest

from LoveLetter.cards import (
    Baron,
    Countess,
    Guard,
    Handmaiden,
    King,
    Priest,
    Prince,
    Princess
)


@pytest.mark.parametrize("card_type, expected_value", [
    (Guard, 1),
    (Priest, 2),
    (Baron, 3),
    (Handmaiden, 4),
    (Prince, 5),
    (King, 6),
    (Countess, 7),
    (Princess, 8),
])
def test_card_value(card_type, expected_value):
    card = card_type()
    assert card.value == expected_value


@pytest.mark.parametrize("first_card_type, second_card_type, expected_value", [
    (Guard, Guard, False),
    (Guard, Baron, True),
    (Countess, Handmaiden, False),
])
def test_card_comparison(first_card_type, second_card_type, expected_value):
    first_card = first_card_type()
    second_card = second_card_type()
    assert (first_card < second_card) == expected_value


# Write tests for each card effect
