import sys

from game import Game
from LoveLetter.player import Human, IA
import colors

players = []
players_names = ["Player 1", "Player 2", "Player 3", "Player 4"]
colors = [colors.blue, colors.green, colors.red, colors.dark_violet]


def exit_game(message):
    print(message)
    sys.exit()


if len(players_names) > 4:
    exit_game("Maximum player allowed for this game is 4")
elif len(players_names) < 2:
    exit_game("Minimum player allowed for this game is 2")


for player_name in players_names:
    players.append(Human(player_name, colors.pop()))

Game(players).run()
