from abc import ABC, abstractmethod

from LoveLetter.cards import (
    Countess,
    King,
    Prince,
    Princess,
)


class Player:
    height = 100

    def __init__(self, name, color):
        self.name = name
        self.hand = []
        self.immune = False
        self.dead = False
        self.color = color

    def __repr__(self):
        return self.name

    def get_card(self):
        return self.hand[0]

    def get_hand(self):
        return self.hand

    # Are all other players immune ?
    def has_targets(self):
        immunities = [
            player.immune
            for player in self.game.players_in_line
            if player != self
        ]
        return not all(immunities)

    # cf guard action() method
    def has_card(self, guess):
        if self.get_card().value == guess:
            return True
        return False

    def discard(self):
        # Discard the Princess makes you lose
        if type(self.get_card()) == Princess:
            self.get_eliminated()
        else:
            self.hand = []
            self.draw_card()

    def throw(self, card):
        self.hand.remove(card)

    def draw_card(self):
        deck = self.game.deck
        if len(deck) > 0 and len(self.get_hand()) < 2:
            card = deck.pick_card()
            card.set_owner(self)
            self.hand += [card]

    def play(self, **kwargs):
        try:
            card = kwargs["card"]
            card.play_allowed(**kwargs)
            self.hand.remove(card)
            card.action(**kwargs)
        except ValueError:
            pass

        except Exception as e:
            raise e

    def make_turn(self, card, target):
        # If immune, new turn means no more immunity
        if self.immune:
            self.immune = False

        # Draw card
        self.draw_card()
        self.play(card, target)

    def get_eliminated(self):
        self.immune = False
        self.dead = True
        self.game.eliminate(self)

    def swap_hand(self, player):
        self.hand, player.hand = player.hand, self.hand
        for card in self.hand:
            card.set_owner(self)
        for card in player.hand:
            card.set_owner(player)


class Human(Player):

    def __init__(self, name, color):
        Player.__init__(self, name, color)


class IA(Player):

    def __init__(self, name, color):
        Player.__init__(self, name, color)
