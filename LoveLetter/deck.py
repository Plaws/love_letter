from random import sample

from LoveLetter.cards import (
    Baron,
    Countess,
    Guard,
    Handmaiden,
    King,
    Priest,
    Prince,
    Princess
)

DEFAULT_DECK_COMPOSITION = {
    Guard: 5,
    Priest: 2,
    Baron: 2,
    Handmaiden: 2,
    Prince: 2,
    King: 1,
    Countess: 1,
    Princess: 1,
}


class Deck:

    def __init__(self, game, deck_composition=DEFAULT_DECK_COMPOSITION):
        self.game = game
        game.set_deck(self)
        self.deck_composition = deck_composition
        self.cards = []
        self.secret_card = None

        for card_type, amount in self.deck_composition.items():
            self.cards += [card_type() for n in range(amount)]

        self.remove_secret_card()

    def __len__(self):
        return len(self.cards)

    def shuffle(self):
        self.cards = sample(self.cards, len(self.cards))

    def pick_card(self):
        if self.cards:
            return self.cards.pop()

    def remove_secret_card(self):
        if self.secret_card is None:
            self.secret_card = self.pick_card()
        else:
            print("A secret card has already been removed from the game.")
