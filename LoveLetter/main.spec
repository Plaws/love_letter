# -*- mode: python -*-

block_cipher = None

a = Analysis(['main.py'],
             pathex=['.'],
             binaries=[],
             datas=[
              ('./cards_image/guard.jpg', './cards_image/'),
              ('./cards_image/priest.jpg', './cards_image/'),
              ('./cards_image/baron.jpg', './cards_image/'),
              ('./cards_image/handmaiden.jpg', './cards_image/'),
              ('./cards_image/prince.jpg', './cards_image/'),
              ('./cards_image/king.jpg', './cards_image/'),
              ('./cards_image/countess.jpg', './cards_image/'),
              ('./cards_image/princess.jpg', './cards_image/'),

             ],
             hiddenimports=['six', 'packaging', 'packaging.version', 'packaging.specifiers', 'packaging.requirements'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='main',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='main')
