from collections import deque
from math import ceil
from time import sleep
import pygame

from LoveLetter.cards import (
    Guard, Priest, Baron, Handmaiden,
    Prince, King, Countess, Princess,
    Card, RequiresTarget, RequiresGuess, CountessException, InvalidTarget
)
from LoveLetter.colors import pink, white, black
from LoveLetter.deck import Deck
from LoveLetter.gui_tools import Pane
from LoveLetter.player import Human, IA


class Game:

    def __init__(self, players, display_width=1280, display_height=980,
                 caption="Love Letter"):
        pygame.init()
        self.display_width = display_width
        self.display_height = display_height
        self.caption = caption
        self.display_board()
        self.players = players
        self.players_in_line = deque(players)
        self.played_card = None
        self.discard = None
        self.target = None
        self.guess = None
        self.guess_choices = []
        self.game_over = False

    def set_deck(self, deck):
        self.deck = deck

    def eliminate(self, player):
        print("{} is out of the game.".format(player))
        self.players_in_line.remove(player)

    def init(self):
        self.deck.shuffle()

        for player in self.players:
            player.draw_card()

    def display_board(self):
        self.game_display = pygame.display.set_mode(
            (self.display_width, self.display_height))
        pygame.display.set_caption(self.caption)

    def display_card(self, card, x, y):
        card_image = pygame.image.load(card.image)
        card_image = pygame.transform.scale(card_image, card.image_proportion)
        card.area = self.game_display.blit(card_image, (x, y))

    def display_guess_choices(self):
        card_types = [Priest(), Baron(), Handmaiden(), Prince(),
                      King(), Countess(), Princess()]
        guess_pane_height = 40
        guess_pane_width = 350

        for i in range(len(card_types)):
            card_type = card_types[i]
            pane_coord = (
                self.display_width - guess_pane_width,
                self.display_height - guess_pane_height - i * guess_pane_height, #noqa
                guess_pane_width,
                guess_pane_height,
            )
            pane_color = card_type.color
            pane_text = str.encode(str(card_type))
            pane_text_size = 20
            guess_pane = Pane(
                screen=self.game_display,
                coord=pane_coord,
                color=pane_color,
                text=pane_text,
                text_size=pane_text_size,
            )
            guess_pane.guess = card_type.value
            guess_pane.display()
            self.guess_choices.append(guess_pane)

    def display_remaining_cards_number(self):
        remaining_cards_coord = (
            (self.display_width / 4) * 3 - Card.card_width / 2,
            self.display_height / 2 - Card.card_height / 2,
            Card.card_width,
            Card.card_height
        )

        remaining_cards_pane = Pane(
            screen=self.game_display,
            coord=remaining_cards_coord,
            color=pink,
            text="x{}".format(len(self.deck)),)
        remaining_cards_pane.display()

    def display_discard(self, card):
        if card:
            card_image = pygame.image.load(card.image)
            card_image = pygame.transform.scale(card_image,
                                                card.image_proportion)
            discard_area = self.game_display.blit(
                card_image,
                (self.display_width / 4 - card.card_width,
                 self.display_height / 2 - card.card_height / 2)
            )

    def display_players(self, pov):
        player_width = ceil(self.display_width / len(self.players))
        i = 0
        indicator_font = pygame.font.SysFont('Arial', 18)
        for player in self.players:
            pane_coord = (
                player_width * i,
                0,
                player_width,
                player.height,
            )
            pane_color = player.color
            pane_text = player.name
            pane_text_size = 20
            player_pane = Pane(
                screen=self.game_display,
                coord=pane_coord,
                color=pane_color,
                text=pane_text,
                text_size=pane_text_size,
            )
            player_pane.display()
            player.area = player_pane.area
            if player.immune:
                indicator = indicator_font.render("P", True, (0, 0, 0))
                self.game_display.blit(indicator, (player_width * i + 10, 10))
            if player.dead:
                indicator = indicator_font.render("X", True, (0, 0, 0))
                self.game_display.blit(indicator, (player_width * i + 10, 10))
            if player == pov:
                # Draw border
                border = Pane(
                    screen=self.game_display,
                    coord=pane_coord,
                    color=black,
                    border=3,
                )
                border.display()
            i += 1

    def make_turn(self):
        if self.played_card:
            if type(self.played_card) == Guard:
                self.display_guess_choices()

            try:
                # If no target, you have to throw a card.
                if self.playing_player.has_targets():
                    self.playing_player.play(
                        card=self.played_card,
                        target=self.target,
                        guess=self.guess,)
                else:
                    self.playing_player.throw(self.played_card)

                self.discard = self.played_card
                self.played_card = None
                self.target = None
                self.guess = None
                self.playing_player = self.players_in_line.popleft()
                if self.playing_player.immune:
                    print("End of immunity for {}".format(self.playing_player))
                    self.playing_player.immune = False
                self.players_in_line.append(self.playing_player)

                # Make a last turn
                if len(self.deck) == 0:
                    self.make_turn()
                    self.end_game()

                if len(self.players_in_line) == 1:
                    self.end_game()

            except CountessException as e:
                print(e.message)

            except RequiresTarget as e:
                print(e.message)

            except RequiresGuess as e:
                print(e.message)

            except InvalidTarget as e:
                print(e.message)

    def end_game(self):
        # A player eliminated everybody else
        if len(self.players_in_line) == 1:
            print("{} wins the game because (s)he is the only survivor."
                  .format(self.players_in_line[0]))

        # There are no cards anymore
        # therefore the player with the highest value wins
        if len(self.deck) == 0:
            highest_value = 0
            for player in self.players_in_line:
                player_value = player.get_card().value
                if player_value > highest_value:
                    highest_value = player_value
                    winner = player
            print("{} wins the game because (s)he has "
                  "the highest card value."
                  .format(winner))

        self.game_over = True

    def run(self):
        for player in self.players:
            player.game = self

        love_letter_deck = Deck(self)
        self.init()

        clock = pygame.time.Clock()
        crashed = False

        self.playing_player = self.players_in_line.popleft()
        self.players_in_line.append(self.playing_player)

        while not crashed and not self.game_over:
            self.game_display.fill(white)

            hand = self.playing_player.get_hand()
            self.playing_player.draw_card()

            self.display_players(pov=self.playing_player)

            # Display cards
            for i in range(len(hand)):
                self.display_card(
                    hand[i],
                    self.display_width / 2 - hand[i].card_width + i * hand[i].card_width, # noqa
                    self.display_height - hand[i].card_height,
                )

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    crashed = True

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    mouse_pos = pygame.mouse.get_pos()
                    for target in self.players:
                        try:
                            if target.area.collidepoint(mouse_pos):
                                self.target = target
                        except AttributeError:
                            pass

                    for card in hand:
                        if card.area.collidepoint(mouse_pos):
                            self.played_card = card

                    for guess in self.guess_choices:
                        if guess.area.collidepoint(mouse_pos):
                            self.guess = guess.guess

            self.make_turn()
            self.display_remaining_cards_number()
            self.display_discard(self.discard)
            pygame.display.update()
            clock.tick(60)

        pygame.quit()
