black = (0, 0, 0)
blue = (102, 153, 255)
dark_orange = (255, 153, 51)
dark_pink = (255, 0, 102)
dark_violet = (163, 0, 204)
green = (51, 204, 51)
grey_blue = (221, 221, 238)
light_blue = (102, 204, 255)
pale_yellow = (255, 255, 204)
pink = (255, 192, 203)
red = (255, 0, 0)
white = (255, 255, 255)
