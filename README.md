# Love Letter

This project is a clone of Love Letter in Python3.

/!\ It is not working yet because it is still in development.

## Rules

Rules can be found on [Wikipedia](https://en.wikipedia.org/wiki/Love_Letter_(card_game))

## Getting Started

To get a copy of the project on your machine, just run :

```
git clone git@framagit.org:Plaws/love_letter.git
```

### Prerequisites

The program needs the following packages to run :

* pygame

To run the tests you also need to have the following packages installed :

* pytest

To build the program you need :

* pyinstaller

All the prerequisites can be installed by running

```
pip install love_letter
```

## Installing

_The following instructions have only been tested on Linux._

Build the package from sources :

```
cd love_letter
pyinstaller main.spec
```

Run the executable :

```
./dist/main/main
```

## Running the tests

To run the tests just run this command :

```
pytest -v tests/*
```

## Contributing

/!\ WIP: Need to write a CONTRIBUTING.md file

## Authors

* **Jef Roelandt** - *Initial work* - [Plaws](https://framagit.org/Plaws)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
