from setuptools import setup, find_packages
from codecs import open
from os import path


setup(
    name='Love Letter',
    version='0.0.1',
    description='Love Letter clone in Python',
    url='https://framagit.org/Plaws/love_letter',
    author='Jef Roelandt',
    author_email='roelandt.jef@protonmail.com',
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Intended Audience :: Developers',
    ],
    install_requires=[
        "pygame",
        "pytest",
        "pyinstaller",
    ],
    entry_points={
        'console_scripts': [
        ]
    }
)
